import 'package:flutter/material.dart';
import 'package:fruit_listview/src/models/fruit_data_model.dart';
import 'package:fruit_listview/src/pages/fruit_detail.dart';
import 'package:fruit_listview/src/pages/home_page.dart';

class MenuList extends StatefulWidget {
  const MenuList({Key? key}) : super(key: key);

  @override
  State<MenuList> createState() => _MenuListState();
}

class _MenuListState extends State<MenuList> {

  static List<String> fruitname = [
    'Apple',
    'Banana',
    'Mango',
    'Orange',
    'Pineapple'
  ];
  static List url = [
    'https://www.applesfromny.com/wp-content/uploads/2020/05/Jonagold_NYAS-Apples2.png',
    'https://cdn.mos.cms.futurecdn.net/42E9as7NaTaAi4A6JcuFwG-1200-80.jpg',
    'https://media.istockphoto.com/photos/mango-isolated-on-white-background-picture-id911274308?k=20&m=911274308&s=612x612&w=0&h=YY8-xqycxsqFea5B-JdhlcgExlXYWMiFoLJdQ-LUx5E=',
    'https://5.imimg.com/data5/VN/YP/MY-33296037/orange-600x600-500x500.jpg',
    'https://5.imimg.com/data5/GJ/MD/MY-35442270/fresh-pineapple-500x500.jpg'
  ];

  static List description = [
    'Apple is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Banana is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Mango is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Orange is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Pineapple is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry'
  ];

  final List<FruitDataModel> Fruitdata = List.generate(
      fruitname.length,
          (index) => FruitDataModel(
          '${fruitname[index]}', '${url[index]}', '${description[index]}'));

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        padding: EdgeInsets.zero,
        child: Column(
            children: [
          UserAccountsDrawerHeader(
          accountName: Text('Suebsakun Khongchang',style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
          accountEmail: Text('6350110025@psu.ac.th'),
          currentAccountPicture: CircleAvatar(
            child: ClipOval(
              child: Image.asset('images/profile1.png',
                width: 90,
                height: 90,
                fit: BoxFit.cover,
              ),
            ),
          ),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage('https://cdn.pixabay.com/photo/2018/08/14/13/23/ocean-3605547__480.jpg',
              ),
              fit: BoxFit.cover,
            ),
          ),
        ),
              Container(
                height: 80,
                child: Card(
                  child: Align(
                    alignment: Alignment.center,
                    child: ListTile(
                      leading: SizedBox(
                        width: 70,
                        height: 70,
                        child: Icon(Icons.home,size: 40,),
                      ),
                      title: const Text('Home',style: TextStyle(fontSize: 18),),
                      onTap: () {
                        Navigator.push(
                            context, MaterialPageRoute(builder: (context) => HomePage(title: 'Fruit List Home',)),
                        );
                      },
                    ),
                  ),
                ),
              ),
              
              Container(
                child: ListView.builder(
                  shrinkWrap: true,
                    itemCount: Fruitdata.length,
                    itemBuilder: (context,index) {
                      return Container(
                        height: 80,
                        child: Card(
                          child: Align(
                            alignment: Alignment.center,
                            child: ListTile(
                              title: Text(
                                Fruitdata[index].name,
                                style: TextStyle(fontSize: 18),
                              ),
                              leading: SizedBox(
                                width: 70,
                                height: 70,
                                child: Image.network(Fruitdata[index].ImageUrl),
                              ),
                              onTap: (){
                                Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (context) => FruitDetail(fruitdatamodel: Fruitdata[index])
                                    )
                                );
                              },
                            ),
                          ),
                        ),
                      );
                    },
                  ),
              ),


              // ListView.builder(
              //   shrinkWrap: true,
              //   itemCount: Fruitdata.length,
              //   itemBuilder: (context,index) {
              //     return Container(
              //       height: 80,
              //       child: Card(
              //         child: Align(
              //           alignment: Alignment.center,
              //           child: ListTile(
              //             title: Text(
              //               Fruitdata[index].name,
              //               style: TextStyle(fontSize: 18),
              //             ),
              //             leading: SizedBox(
              //               width: 70,
              //               height: 70,
              //               child: Image.network(Fruitdata[index].ImageUrl),
              //             ),
              //             onTap: (){
              //               Navigator.of(context).push(
              //                   MaterialPageRoute(
              //                       builder: (context) => FruitDetail(fruitdatamodel: Fruitdata[index])
              //                   )
              //               );
              //             },
              //           ),
              //         ),
              //       ),
              //     );
              //   },
              // ),
      ],

        ),
      ),





      );
  }
}
