import 'package:flutter/material.dart';
import 'package:fruit_listview/src/models/fruit_data_model.dart';

class FruitDetail extends StatelessWidget {
  final FruitDataModel fruitdatamodel;
  const FruitDetail({Key? key, required this.fruitdatamodel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(fruitdatamodel.name),
      ),
      body: Column(
        children: [
          Image.network(fruitdatamodel.ImageUrl),
          SizedBox(
            height: 10,
          ),
          Text(
            fruitdatamodel.desc,
            style: TextStyle(fontSize: 20),
          )
        ],
      ),
    );
  }
}
